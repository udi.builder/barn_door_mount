/*
 * interrupt_tim2.c
 *
 *  Created on: 26 апр. 2014 г.
 *      Author: igor
 */
#include "step_motor.h"
#include "menu.h"

void TIM2_IRQHandler(void){
	static uint_fast8_t step;
	if ((MOTOR_TIMER->SR != 0) && (MOTOR_TIMER->DIER != 0)){
		MOTOR_TIMER->SR=(uint16_t)~TIM_IT_Update;
		if (step>6) step=0;
		else step++;
		switch(step){
		case 0:
			GPIOB->ODR&=~STEP_PORT;
			if (menu_number==down) GPIOB->ODR|=STEP4;
			else GPIOB->ODR|=STEP1;
			break;
		case 1:
			GPIOB->ODR&=~STEP_PORT;
			if (menu_number==down) GPIOB->ODR|=STEP4 | STEP3;
			else GPIOB->ODR|=STEP1 | STEP2;
			break;
		case 2:
			GPIOB->ODR&=~STEP_PORT;
			if (menu_number==down) GPIOB->ODR|=STEP3;
			else GPIOB->ODR|=STEP2;
			break;
		case 3:
			GPIOB->ODR&=~STEP_PORT;
			if (menu_number==down) GPIOB->ODR|=STEP2 | STEP3;
			else GPIOB->ODR|=STEP2 | STEP3;
			break;
		case 4:
			GPIOB->ODR&=~STEP_PORT;
			if (menu_number==down) GPIOB->ODR|=STEP2;
			else GPIOB->ODR|=STEP3;
			break;
		case 5:
			GPIOB->ODR&=~STEP_PORT;
			if (menu_number==down) GPIOB->ODR|=STEP2 | STEP1;
			else GPIOB->ODR|=STEP3 | STEP4;
			break;
		case 6:
			GPIOB->ODR&=~STEP_PORT;
			if (menu_number==down) GPIOB->ODR|=STEP1;
			else GPIOB->ODR|=STEP4;
			break;
		case 7:
			GPIOB->ODR&=~STEP_PORT;
			if (menu_number==down) GPIOB->ODR|=STEP1 | STEP4;
			else GPIOB->ODR|=STEP4 | STEP1;
			break;
		}
	}
}


