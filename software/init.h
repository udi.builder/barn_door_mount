#ifndef INIT_HPP_
#define INIT_HPP_

#ifdef __cplusplus
extern "C" {
#endif

void Begin_config(void);
void Final_config(void);
void adcConfigure(void);

#ifdef __cplusplus
}
#endif
#endif /* INIT_HPP_ */
