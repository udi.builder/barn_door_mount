#include "init.h"
//#include <misc.h>
#include "common.h"
#include <stm32f10x.h>
#include "interrupt_dma1_ch1.h"

GPIO_InitTypeDef GPIO_InitStructure;
DMA_InitTypeDef DMA_InitStructure;
TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
NVIC_InitTypeDef NVIC_InitStructure;
EXTI_InitTypeDef EXTI_InitStructure;
TIM_OCInitTypeDef TIM_OCInitStructure;
USART_InitTypeDef USART_InitStructure;
SPI_InitTypeDef SPI_InitStructure;

void Begin_config(void){
	// начальный этап конфигурации
//	RCC_PCLK2Config(RCC_HCLK_Div1); // APB2 = 24MHz
//	RCC->CFGR &= ~RCC_CFGR_PPRE2_DIV16; // APB2 = 24MHz
	NVIC_SetVectorTable(NVIC_VectTab_FLASH,0);
	// по умолчанию
	RCC->APB2ENR |= RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC;
	GPIOA->CRL = 0x88888888;
	GPIOA->CRH = 0x88888888;
	GPIOB->CRL = 0x88888888;
	GPIOB->CRH = 0x88888888;
	GPIOC->CRL = 0x88888888;
	GPIOC->CRH = 0x88888888;
}

void Final_config(void){
	  // заключительный этап конфигурации
	  // приоритеты
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 8;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void adcConfigure(void){
	RCC->APB2ENR |= RCC_APB2Periph_GPIOA | RCC_APB2Periph_ADC1;
	RCC->AHBENR |= RCC_AHBPeriph_DMA1;
	RCC->CFGR |= RCC_CFGR_ADCPRE_DIV8;
	GPIOA->CRL &= ~(GPIO_CRL_CNF3 | GPIO_CRL_MODE3);
	DMA1_Channel1->CPAR=(uint32_t)&ADC1->DR;
	DMA1_Channel1->CMAR=(uint32_t)&adc_buf[0];
	DMA1_Channel1->CNDTR=32;
	DMA1_Channel1->CCR= DMA_CCR1_PL | DMA_CCR1_MSIZE_0 | DMA_CCR1_PSIZE_0 | DMA_CCR1_MINC | DMA_CCR1_CIRC;
	ADC1->SQR1 = 0x00F18C63;
	ADC1->SQR2 = 0x06318C63;
	ADC1->SQR3 = 0x06318C63;
	ADC1->SMPR1 = 0x00FFFFFF;
	ADC1->SMPR2 = 0x3FFFFFFF;
	ADC1->CR1 = ADC_CR1_SCAN ;
	ADC1->CR2 = ADC_CR2_EXTTRIG | ADC_CR2_EXTSEL | ADC_CR2_DMA | ADC_CR2_CONT | ADC_CR2_ADON;
	ADC1->CR2 |= ADC_CR2_RSTCAL;
	while (!(ADC1->CR2 & ADC_CR2_RSTCAL));
	ADC1->CR2 |= ADC_CR2_CAL;
	while (!(ADC1->CR2 & ADC_CR2_CAL));
	DMA1_Channel1->CCR |= DMA_CCR1_HTIE | DMA_CCR1_TCIE | DMA_CCR1_EN;
	ADC1->CR2 |= ADC_CR2_SWSTART;
}
