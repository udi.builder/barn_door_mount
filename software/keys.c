/*
 * keys.c
 *
 *  Created on: 29 апр. 2014 г.
 *      Author: igor
 */

#include "keys.h"
#include "common.h"

KEY key_r, key_l;

void keysInit(void){
	RCC->APB2ENR |= RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB;
	GPIOA->CRL &= ~(GPIO_CRL_CNF0_1 | GPIO_CRL_CNF1_1 | GPIO_CRL_MODE0 | GPIO_CRL_MODE1);
	GPIOA->CRL |= (GPIO_CRL_CNF0_0 | GPIO_CRL_CNF1_0);
	GPIOB->CRH &= ~(GPIO_CRH_CNF9_1 | GPIO_CRH_MODE9);
	GPIOB->CRH |= (GPIO_CRH_CNF9_0);
}

void keysRefresh(void){
	if ((GPIOA->IDR & GPIO_Pin_0)!=0) key_r.up=0;
	else key_r.up=1;
	if ((GPIOA->IDR & GPIO_Pin_1)!=0) key_r.down=0;
	else key_r.down=1;
	if ((GPIOB->IDR & GPIO_Pin_9)!=0) key_r.set=0;
	else key_r.set=1;
	if (key_r.byte!=key_l.byte){
		key_l.byte=key_r.byte;
		flag.key_change=1;
	}
	else{
		if (flag.key_change==1){
			flag.key_change=0;
			flag.key_new=1;
		}
	}
}
