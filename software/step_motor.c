/*
 * step_motor.c
 *
 *  Created on: 26 апр. 2014 г.
 *      Author: igor
 *      оборот большого колеса к обороту мотора = 4.05(3)
 *      расстояние от оси до винта = 276.5
 *      1 оборот колеса = 0.2072171 гр. = 746"
 *      746" или 1 оборот за = 49.732 с
 *      1 оборот мотора = 12.2694 с
 *      1 полушаг = 30673.5 мкс
 *      скорость задаем в сек на оборот колеса,
 *      для получения длительности полушага - скорость делим на 1.62133
 */

#include "step_motor.h"

void stepInit(void){
	RCC->APB2ENR |= RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO;
	AFIO->MAPR |= AFIO_MAPR_SWJ_CFG_NOJNTRST;
	GPIOB->CRL &= ~(GPIO_CRL_CNF4 | GPIO_CRL_CNF5 | GPIO_CRL_CNF6 | GPIO_CRL_CNF7);
	GPIOB->CRL |= (GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6 | GPIO_CRL_MODE7);
	GPIOB->ODR &= ~STEP_PORT;
	RCC->APB1ENR |= RCC_APB1Periph_TIM2;
	MOTOR_TIMER->ARR=12731; // 1ms
	MOTOR_TIMER->PSC=23; // 1тик = 1мкс
	MOTOR_TIMER->CR2=0;
	MOTOR_TIMER->DIER = TIM_DIER_UIE;
	MOTOR_TIMER->CR1=TIM_CR1_CEN;
}

void speedChange(uint16_t value){
	// изменение скорости
	if (value!=0){
		value=(uint32_t)(float)value/1.62133;
		MOTOR_TIMER->ARR=value;
		MOTOR_TIMER->DIER = TIM_DIER_UIE;
	}
	else{
		MOTOR_TIMER->DIER = 0;
		GPIOB->ODR &= ~STEP_PORT;
	}
}
